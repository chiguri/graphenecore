package info.chiguri.graphene.argumentation;

import info.chiguri.graphene.canvas.AbstractColor;

public class DisplayConfig {
	public static int ArgumentRadius = 10;
	public static int ArgumentFontSize = 8;

	public static AbstractColor ArgumentEdgeColor = null;

	public static AbstractColor InColor = null;
	public static AbstractColor OutColor = null;
	public static AbstractColor UndecColor = null;

	public static AbstractColor ValidAttackColor = null;
	public static AbstractColor InValidAttackColor = null;
	public static AbstractColor UnclearAttackColor = null;
}
