package info.chiguri.graphene.argumentation;

public class UnsatisifiablePartialLabelingException extends RuntimeException {
	public UnsatisifiablePartialLabelingException(String str) {
		super("Partial Labeling cannot be satisified: " + str);
	}
}
