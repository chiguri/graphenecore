package info.chiguri.graphene.graph;

public class PlanarRepositioning<G extends NodeInterface, E extends Edge<G>> extends AbstractRepositioning<G, E> {
	protected boolean processed = false;

	protected PlanarRepositioning(Graph<G, E> g) {
		super(g);
	}

	protected PlanarRepositioning(Graph<G, E> g, boolean isConnected) {
		super(g, isConnected);
	}

	@Override
	public boolean finish() {
		return processed;
	}

	@Override
	protected void process() {
		// TODO 自動生成されたメソッド・スタブ
		Graph<G, Edge<G>> g = new Graph<>();
		this.g.cloning(g, v -> v, (map, e) -> new Edge<>(map.get(e.from), map.get(e.to))); // Just cloning graph with the same point (to add some virtual edges)

		processed = true;
	}

	@Override
	public void enlarge(double ratio) {
		// This does nothing because enlarging graph do not effect drawing itself
	}

}
