package info.chiguri.graphene.graph;

import info.chiguri.graphene.canvas.AbstractCanvas;

public class CircleNode extends AbstractNode {
	protected int r;

	CircleNode() {
		r = 2;
	}
	public CircleNode(int r, Point p) {
		super(p);
		this.r = r;
	}
	public CircleNode(int r, int x, int y) {
		super(x, y);
		this.r = r;
	}

	@Override
	public boolean inside(int x, int y) {
		return (p.x-x)*(p.x-x)+(p.y-y)*(p.y-y)<=r*r;
	}

	@Override
	public Point getEdgePoint(Point p) {
		if(this.p.equals(p)) return p;
		double dx = p.x - this.p.x;
		double dy = p.y - this.p.y;
		double d = Math.sqrt(dx*dx+dy*dy);
		double sine = dy/d;
		double cosine = dx/d;
		return new Point(this.p.x + (int)(r * cosine), this.p.y + (int)(r * sine));
	}

	@Override
	public void draw(AbstractCanvas canvas) {
		canvas.drawOval((int)(p.x-r), (int)(p.y-r), r*2, r*2);
	}
}
