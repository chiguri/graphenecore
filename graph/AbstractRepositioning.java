package info.chiguri.graphene.graph;

abstract public class AbstractRepositioning<G extends NodeInterface, E extends Edge<G>> {
	public static int minStep = 10;

	protected final Graph<G, E> g;
	protected final int targetRevision;
	protected final boolean isConnected;

	protected AbstractRepositioning(Graph<G, E> g) {
		this.g = g;
		targetRevision = g.getRevision();
		isConnected = true;
		// Basically, Repositioning algorithm is limited for connected graphs. If the repositioning algorithm allows disconnected ones, use the other constructor with isConnected=false
		if(!g.isConnected()) {
			throw new DisconnectedGraphException(g);
		}
	}

	protected AbstractRepositioning(Graph<G, E> g, boolean isConnected) {
		this.g = g;
		targetRevision = g.getRevision();
		this.isConnected = isConnected;
		if(isConnected && !g.isConnected()) {
			throw new DisconnectedGraphException(g);
		}
	}

	abstract public boolean finish();
	public final void step() {
		if(targetRevision == g.getRevision()) {
			process();
		}
		else {
			// do nothing ... is it better to raise exception?
		}
	}
	abstract protected void process();
	abstract public void enlarge(double ratio);

	public void repositioning(int maxCount) {
		if(maxCount < minStep) {
			for(int i = 0; i < maxCount; ++i) {
				step();
			}
		}
		else {
			for(int i = 0; i < minStep; ++i) {
				step();
			}
			for(int i = minStep; i < maxCount; ++i) {
				if(finish()) {
					break;
				}
				step();
			}
		}
	}
	final public void repositioning() {
		repositioning(2000000);
	}
}
