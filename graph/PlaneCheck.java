package info.chiguri.graphene.graph;

import info.chiguri.graphene.IterableIterator;

// This class gives methods to judge "a graph IS drawn as plane graph", but not "a graph is planar (i.e., CAN BE drawn as plane graph)"
public class PlaneCheck {
	public static boolean crossing(Point p1, Point p2, Point p3, Point p4) {
		if(p1.equals(p3) || p1.equals(p4) || p2.equals(p3) || p2.equals(p4)) {
			return false; // 同じ点を共有している辺は交差すると判定しない
		}
		// 判定法http://www5d.biglobe.ne.jp/~tomoya03/shtml/algorithm/Intersection.htm
		// =0になる（＝上に乗っている）ことは計算誤差から考えにくいが、その場合は交差しないとする
		if(((p1.x - p2.x) * (p3.y - p1.y) + (p1.y - p2.y) * (p1.x - p3.x)) *
		   ((p1.x - p2.x) * (p4.y - p1.y) + (p1.y - p2.y) * (p1.x - p4.x)) < 0) {
			if(((p3.x - p4.x) * (p1.y - p3.y) + (p3.y - p4.y) * (p3.x - p1.x)) *
			   ((p3.x - p4.x) * (p2.y - p3.y) + (p3.y - p4.y) * (p3.x - p2.x)) < 0) {
				return true;
			}
		}
		return false;
	}
	// Assume e1 and e2 are segment (straight line)
	public static <G extends NodeInterface> boolean crossing(Edge<G> e1, Edge<G> e2) {
		return crossing(e1.from.getPoint(), e1.to.getPoint(), e2.from.getPoint(), e2.to.getPoint());
	}

	// Is g drawn as plane graph? (not "g is planar")
	public static <G extends NodeInterface, E extends Edge<G>> boolean isPlane(Graph<G, E> g) {
		int num = g.edgeNum();
		@SuppressWarnings("unchecked")
		Edge<G>[] es = new Edge[num];
		int i = 0;
		for(Edge<G> e : new IterableIterator<>(g.edgeIterator())) {
			es[i++] = e;
		}
		for(i = 0; i < num; ++i) {
			for(int j = i+1; j < num; ++j) {
				if(crossing(es[i], es[j])) {
					return false;
				}
			}
		}
		return true;
	}

	private static boolean onSegment(Point t1, Point t2, Point p) {
		if(p.x < t1.x && p.x > t2.x || p.x > t2.x && p.x < t2.x) { // p is between t1 and t2? (x-axis)
			if(p.y < t1.y && p.y > t2.y || p.y > t2.y && p.y < t2.y) {
				return (p.y - t1.y)*(p.x-t2.x) == (p.y-t2.y)*(p.x-t1.x);
			}
		}
		return false;
	}

	private static <G extends NodeInterface> boolean onLine(int x, int y, Edge<G>[] closed, G p) {
		Point t2 = p.getPoint();
		Point t1 = new Point(x, y);
		for(Edge<G> l : closed) {
			if(onSegment(t1, t2, l.from.getPoint())) {
				return true;
			}
		}
		return false;
	}

	private static <G extends NodeInterface> Point getOutsidePoint(Edge<G>[] closed, G p) {
		int min_x, min_y;
		min_x = min_y = 0;
		for(Edge<G> l : closed) {
			info.chiguri.graphene.graph.Point p0 = l.from.getPoint();
			if(p0.x <= min_x) {
				min_x = (int) (p0.x - 1);
			}
			if(p0.y <= min_y) {
				min_y = (int) (p0.y - 1);
			}
		}
		// (min_x, min_y) is outside of "closed", but some points on "closed" may be on the segment from it and p
		// n-times tries may avoid this situation
		while(onLine(min_x, min_y, closed, p)) {
			--min_y;
		}
		return new Point(min_x, min_y);
	}

	public static <G extends NodeInterface> boolean isInside(Edge<G>[] closed, G p) {
		// assumption: any two edges in "closed" should not be crossed each other
		// 外部との点との線分は必ず交差が奇数回になる。
		// 点上で交差すると厄介なので事前にそうならないような点を外部に作る（getOutsidePointによる）
		if(closed[0].from != closed[closed.length-1].to) {
			throw new RuntimeException("Edges are not closed");
		}
		int cross = 0;

		Point p1 = p.getPoint();
		for(Edge<G> l : closed) {
			if(l.from.getPoint().equals(p1)) {
				// if p is the same point with any point in "closed", it is false
				return false;
			}
		}
		for(Edge<G> l : closed) {
			if(onSegment(l.from.getPoint(), l.to.getPoint(), p1)) {
				// if p is on the segment in "closed", it is false
				return false;
			}
		}

		Point p2 = getOutsidePoint(closed, p);

		for(Edge<G> l : closed) {
			if(PlaneCheck.crossing(p1, p2, l.from.getPoint(), l.to.getPoint())) {
				++cross;
			}
		}

		return cross % 2 == 1;
	}
}
