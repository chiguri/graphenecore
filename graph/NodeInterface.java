package info.chiguri.graphene.graph;

import info.chiguri.graphene.canvas.AbstractCanvas;

public interface NodeInterface {
	/**
	 * Check whether the given point is in the node (when the node is drawn as figure).
	 * @param x : x-coordinate of the checking point
	 * @param y : y-coordinate of the checking point
	 * @return whether (x, y) is in the node-figure
	 */
	abstract public boolean inside(int x, int y);

	/**
	 * Indicate whether the given point is in the node (when the node is drawn as figure).
	 * @param p : the checking point
	 * @return whether p is in the node-figure
	 */
	default public boolean inside(Point p) {
		return inside(p.x, p.y);
	}

	/**
	 * Move the node to the given point.
	 * @param p : point to move
	 */
	abstract public void moveTo(Point p);

	/**
	 * Move the node to the given point.
	 * @param x : x-coodinate of the point to move
	 * @param y : y-coodinate of the point to move
	 */
	default public void moveTo(int x, int y) {
		moveTo(new Point(x, y));
	}

	/**
	 * Generate crossing point with the edge of the node and the line from the center of the node to the given point.
	 * Note that whether the returned point is "inside" of the node is unspecified.
	 * @param p : target point for drawing line from the center of the node
	 * @return the point crossing with the edge of the node and the line from the center to p
	 */
	abstract public Point getEdgePoint(Point p);

	/**
	 * Generate crossing point with the edge of the node and the line from the center of the node to that of the given node.
	 * Note that whether the returned point is "inside" of the node is unspecified.
	 * @param p : target point for drawing line from the center of the node
	 * @return the point crossing with the edge of the node and the line from the center of the node to that of n
	 */
	default public Point getEdgePoint(NodeInterface n) {
		return getEdgePoint(n.getPoint());
	}

	/**
	 * @return the center of the node
	 */
	abstract public Point getPoint();

	/**
	 * Drawing node into the canvas.
	 * @param canvas : the canvas to draw the node
	 */
	abstract public void draw(AbstractCanvas canvas);
}
