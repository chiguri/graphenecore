package info.chiguri.graphene.graph;

public final class Point {
	public final int x, y;
	Point() {
		x = y = 0;
	}
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean equals(Object o) {
		return (o instanceof Point) && ((Point)o).x == x && ((Point)o).y == y;
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
}
