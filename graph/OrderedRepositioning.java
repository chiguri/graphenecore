package info.chiguri.graphene.graph;

import java.util.HashMap;

import info.chiguri.graphene.IterableIterator;

// Graph drawing for "Partial Ordered" graph like tree, lattice, etc.
// This is not enough for "planar drawing", so sometimes make edge-crossing even if the graph is planar
public class OrderedRepositioning<G extends NodeInterface, E extends Edge<G>> extends AbstractRepositioning<G, E> {
	protected boolean processed = false;
	protected int left, right, top, bottom;
	NodeInterface[] nodes;
	int[] heights;
	int[] froms;
	int[] tos;

	public OrderedRepositioning(Graph<G, E> g, int left, int right, int top, int bottom) {
		super(g);
		this.left = left;
		this.right = right;
		this.top = top;
		this.bottom = bottom;

		nodes = new NodeInterface[g.nodeNum()];
		{
			int t = 0;
			for(G v : new IterableIterator<>(g.nodeIterator())) {
				nodes[t++] = v;
			}
		}

		heights = new int[g.nodeNum()];
		froms = new int[g.edgeNum()];
		tos = new int[g.edgeNum()];
		{
			HashMap<NodeInterface, Integer> map = new HashMap<>();
			for(int i = 0; i < nodes.length; ++i) {
				map.put(nodes[i], i);
				heights[i] = 1;
			}
			int t = 0;
			for(E e : new IterableIterator<>(g.edgeIterator())) {
				froms[t] = map.get(e.from);
				tos[t] = map.get(e.to);
				++t;
			}
		}
	}

	protected int[] calculateHeight() {
		boolean modified = false;
		do {
			modified = false;
			for(int i = 0; i < froms.length; ++i) {
				if(froms[i] == tos[i]) continue;
				if(heights[froms[i]] <= heights[tos[i]]) {
					int h = heights[tos[i]] + 1;
					if(h > nodes.length) {
						// height is at most the number of nodes
						throw new RuntimeException("This graph has non-self loops");
					}
					heights[froms[i]] = h;
					modified = true;
				}
			}
		} while(modified);
		return heights;
	}

	protected int[] sortHorizontalOrder(int[] prev, int h) {
		int[] currents = new int[nodes.length];
		int current_num = 0;
		for(int i = 0; i < heights.length; ++i) {
			if(heights[i] == h) {
				currents[current_num] = i;
				++current_num;
			}
		}

		return currents;
	}

	@Override
	public boolean finish() {
		return processed;
	}

	@Override
	protected void process() {
		final int width = right - left;
		final int height = bottom - top;
		if(width == 0 || height == 0) {
			System.err.println("Cannot reposition");
			return;
		}

		// if width/height is not enough, then how do we do?
		int[] heights = calculateHeight();

		int upperHeight = 2;
		for(int i = 0; i < heights.length; ++i) {
			if(heights[i] >= upperHeight) upperHeight = heights[i]+1;
		}

		double[] horRatio = new double[nodes.length];
		for(int i = 0; i < nodes.length; ++i) {
			horRatio[i] = 0.0;
		}
		int[] currents = new int[nodes.length];
		int current_num = 0;
		for(int i = 0; i < heights.length; ++i) {
			if(heights[i] == 1) {
				currents[current_num] = i;
				++current_num;
			}
		}
		for(int i = 0; i < current_num; ++i) {
			horRatio[currents[i]] = (i+1.0)/(current_num+1.0);
		}
		for(int h = 2; h <= upperHeight; ++h) {
			current_num = 0;
			for(int i = 0; i < nodes.length; ++i) {
				if(heights[i] != h) continue;

				currents[current_num] = i;
				++current_num;

				int num_edge = 0;
				for(int j = 0; j < froms.length; ++j) {
					if(froms[j] == i) {
						horRatio[i] += horRatio[tos[j]];
						++num_edge;
					}
				}
				horRatio[i] /= num_edge;
			}
			// sort
			for(int i = 0; i < current_num; ++i) {
				int min_p = i;
				for(int j = i+1; j < current_num; ++j) {
					if(horRatio[currents[min_p]] > horRatio[currents[j]]) {
						min_p = j;
					}
				}
				int t = currents[i];
				currents[i] = currents[min_p];
				currents[min_p] = t;
			}

			for(int i = 0; i < current_num; ++i) {
				horRatio[currents[i]] = (i+1.0)/(current_num+1.0);
			}
		}

		double height_per_num = height/(double)upperHeight;
		for(int i = 0; i < nodes.length; ++i) {
			nodes[i].moveTo((int)(horRatio[i]*width)+left, (int)(heights[i]*height_per_num)+top);
		}

		processed = true;
	}

	@Override
	public void enlarge(double ratio) {
		// do nothing (because there are no parameters for repositioning other than window for placing)

	}

}
