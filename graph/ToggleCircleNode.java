package info.chiguri.graphene.graph;

import info.chiguri.graphene.canvas.AbstractCanvas;
import info.chiguri.graphene.canvas.AbstractColor;

public class ToggleCircleNode extends CircleNode implements ToggleNodeInterface {
	protected boolean mode = false;
	protected AbstractColor c1, c2;

	public ToggleCircleNode(int r, AbstractColor c1, AbstractColor c2, Point p) {
		super(r, p);
		this.c1 = c1; this.c2 = c2;
	}
	public ToggleCircleNode(int r, AbstractColor c1, AbstractColor c2, int x, int y) {
		super(r, x, y);
		this.c1 = c1; this.c2 = c2;
	}

	@Override
	public void draw(AbstractCanvas canvas) {
		AbstractColor c = canvas.getColor();
		if(mode) {
			canvas.setColor(c1);
		}
		else {
			canvas.setColor(c2);
		}
		canvas.fillOval((int)p.x-r, (int)p.y-r, r*2, r*2);
		canvas.setColor(c);
	}

	@Override
	public boolean setMode(boolean b) {
		boolean old = mode;
		mode = b;
		return old;
	}

}
