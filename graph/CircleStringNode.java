package info.chiguri.graphene.graph;

import info.chiguri.graphene.canvas.AbstractCanvas;
import info.chiguri.graphene.canvas.AbstractFont;

public class CircleStringNode extends CircleNode {
	protected String label;
	protected int fsize;

	CircleStringNode() {
		label = "test";
		fsize = 10;
	}
	public CircleStringNode(String label, int r, int fsize, Point p) {
		super(r, p);
		this.label = label;
		this.fsize = fsize;
	}
	public CircleStringNode(String label, int r, int fsize, int x, int y) {
		super(r, x, y);
		this.label = label;
		this.fsize = fsize;
	}

	@Override
	public void draw(AbstractCanvas canvas) {
		super.draw(canvas);
		AbstractFont original = canvas.getFont();
		canvas.setFont(original.deriveFont((float)fsize));
		canvas.drawString(label, (int)p.x-r/2, (int)p.y+r/2); // TODO position adjustment is necessary
		canvas.setFont(original);
	}
}
