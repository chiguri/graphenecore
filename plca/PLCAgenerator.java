package info.chiguri.graphene.plca;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import info.chiguri.graphene.IterableIterator;
import info.chiguri.graphene.Pair;
import info.chiguri.graphene.graph.Graph;
import info.chiguri.graphene.graph.PlaneCheck;

public class PLCAgenerator {
	public final Graph<Point, Line> g;

	private Point[] points;
	private Line[] lines;

	protected final Map<Line, Line> rotNext = new HashMap<>();
	protected final Map<Line, Line> inverse = new HashMap<>();
	protected final Map<Line, Double> rads = new HashMap<>();
	private final Set<Line> rests = new HashSet<>();


	public PLCAgenerator(Graph<Point, Line> g) {
		this.g = g;
	}

	public Expression generatePLCA() {
		if(!PlaneCheck.isPlane(g)) {
			throw new RuntimeException("Graph is not planar; move points or remove points and/or lines");
		}

		clone_graph();

		setup_line();

		setup_rotate();

		Circuit[] cs = resolveCircuits();

		Pair<Circuit, Area[]> pair = resolveAreas(cs);
		Area[] as = pair.snd;
		Circuit outermost = pair.fst;

		Expression exp = new Expression();
		for(Point p : points) {
			exp.addPoint(p);
		}
		for(Line l : lines) {
			Line inv = inverse.get(l);
			exp.addLine(l, inv);
		}
		for(Circuit c : cs) {
			exp.addCircuit(c);
		}
		for(Area a : as) {
			exp.addArea(a);
		}
		exp.setOutermost(outermost);
		return exp;
	}

	protected void clone_graph() {
		HashMap<Point, Point> map = new HashMap<>();
		HashMap<Point, Integer> numbering = new HashMap<>();
		int i = 0;
		points = new Point[g.nodeNum()];
		for(Point p : new IterableIterator<>(g.nodeIterator())) {
			Point c = new Point(p.getPoint().x, p.getPoint().y);
			map.put(p, c);
			points[i++] = c;
			numbering.put(p, i);
		}
		i = 0;
		lines = new Line[g.edgeNum()/2];
		inverse.clear();
		for(Line e : new IterableIterator<>(g.edgeIterator())) {
			if(numbering.get(e.from) > numbering.get(e.to)) {
				continue;
			}
			Line l = new Line(map.get(e.from), map.get(e.to));
			Line inv = new Line(l.to, l.from);
			inverse.put(l, inv);
			inverse.put(inv, l);
			lines[i++] = l;
		}
	}

	protected void setup_line() {
		rests.clear();
		for(Line l : lines) {
			rests.add(l);
			rests.add(inverse.get(l));
		}
	}

	protected void setup_rotate() {
		HashMap<Point, List<Line>> radOrder = new HashMap<>();
		rads.clear();
		rotNext.clear();
		for(Point p : points) {
			radOrder.put(p, new LinkedList<Line>());
		}
		for(Line l : rests) {
			List<Line> t = radOrder.get(l.from);
			double x = l.to.getPoint().x - l.from.getPoint().x;
			double y = l.to.getPoint().y - l.from.getPoint().y;
			double rad = Math.acos(x/Math.sqrt(x*x+y*y));
			if(y < 0) {
				rad = 2 * Math.PI - rad;
			}
			rads.put(l, rad);
			ListIterator<Line> ls = t.listIterator();
			if(!ls.hasNext()) {
				ls.add(l);
			}
			else {
				while(ls.hasNext()) {
					double nrad = rads.get(ls.next());
					if(rad < nrad) {
						ls.previous();
						ls.add(l);
						break;
					}
				}
				if(ls.previous() != l) {
					ls.next();
					ls.add(l);
				}
			}
		}

		for(Point p : points) {
			List<Line> pl = radOrder.get(p);
			Line prev = pl.get(pl.size()-1);
			for(Line l : pl) {
				rotNext.put(prev, l);
				prev = l;
			}
		}
	}

	private Line nextLine(Line prev) {
		return rotNext.get(inverse.get(prev));
	}

	private static Point getLeastPoint(Circuit c) {
		Point p = c.seg.start;
		info.chiguri.graphene.graph.Point pt = p.getPoint();
		for(Line l : c.seg) {
			Point f = l.from;
			info.chiguri.graphene.graph.Point fp = f.getPoint();
			if(pt.y > fp.y || pt.y == fp.y && pt.x > fp.y) {
				p = f;
			}
		}

		return p;
	}

	private static boolean isOuter(Circuit c) {
		Point p = getLeastPoint(c);
		Point from, to;
		from = to = null;
		for(Line l : c.seg) {
			if(l.from == p) {
				to = l.to;
			}
			if(l.to == p) {
				from = l.from;
			}
		}
		if(from != null && to != null) {
			// from-toとfrom-pの外積を計算して方向が正なら外側（右回り）といいたいが座標系の軸の方向が逆なので負で外側
			double a_x = to.getPoint().x - from.getPoint().x;
			double a_y = to.getPoint().y - from.getPoint().y;
			double b_x = p.getPoint().x - from.getPoint().x;
			double b_y = p.getPoint().y - from.getPoint().y;
			double cross = a_x * b_y - a_y * b_x;
			if(cross < 0) {
				return true;
			}
			if(cross > 0) {
				return false;
			}
			throw new RuntimeException("Two lines connected with left-upper-most point may overlap");
		}
		else {
			throw new RuntimeException("Something strange point : " + p.getPoint());
		}
	}

	public Circuit[] resolveCircuits() {
		// Circuitの満たすべき性質（一つのcircuitで行きと帰りが存在しない）は確かめていない。いるかな。
		Vector<Circuit> cs = new Vector<>();

		while(!rests.isEmpty()) {
			Vector<Line> seg = new Vector<>();
			Line first = rests.iterator().next();
			Line current = first;
			do {
				if(!rests.contains(current)) {
					throw new RuntimeException("Something bad graph!: resolveCircuits");
				}
				rests.remove(current);
				seg.add(current);
				current = nextLine(current);
			} while(current != first);

			cs.add(new Circuit(seg.toArray(new Line[0])));
		}
		return cs.toArray(new Circuit[0]);
	}

	private static class CircuitTree {
		Vector<CircuitTree> cs = new Vector<>();
		Line[] c; // this is inner circuit
		Area a;
		CircuitTree(Circuit c, Line[] ls) {
			this.c = ls;
			a = new Area(c);
		}
	}

	private static void traverseTree(CircuitTree t, Vector<Area> as) {
		as.add(t.a);
		for(CircuitTree ct : t.cs) {
			traverseTree(ct, as);
		}
	}

	private static void insertInner(Circuit c, Line[] ls, Vector<CircuitTree> ts) {
		Point p = ls[0].from;
		ListIterator<CircuitTree> iter = ts.listIterator();
		while(iter.hasNext()) {
			CircuitTree t = iter.next();
			if(PlaneCheck.isInside(t.c, p)) { // cがtに包含されている場合
				insertInner(c, ls, t.cs);
				return;
			}
			if(PlaneCheck.isInside(ls, t.c[0].from)) { // cがtを包含している場合（置き換える）
				CircuitTree ct = new CircuitTree(c, ls);
				ct.cs.add(t);
				iter.remove();
				while(iter.hasNext()) { // 他に包含しているものがないか
					t = iter.next();
					if(PlaneCheck.isInside(ls, t.c[0].from)) {
						ct.cs.add(t);
						iter.remove();
					}
				}
				ts.add(ct);
				return;
			}
		}
		ts.add(new CircuitTree(c, ls));
	}

	private static boolean insertOuter(Circuit c, CircuitTree t) {
		Point p = c.seg.iterator().next().from;
		if(PlaneCheck.isInside(t.c, p)) {
			for(CircuitTree ct : t.cs) {
				if(insertOuter(c, ct)) {
					return true;
				}
			}
			// 配下の内側ではなく自分の内側
			t.a.add(c);
			return true;
		}
		return false;
	}

	public static Pair<Circuit, Area[]> resolveAreas(Circuit []cs) {
		Vector<Circuit> outer = new Vector<>();
		Vector<Circuit> inner = new Vector<>();
		for(Circuit c : cs) {
			if(isOuter(c)) {
				outer.add(c);
			}
			else {
				inner.add(c);
			}
		}
		Vector<CircuitTree> ts = new Vector<>();
		for(Circuit c : inner) {
			Line[] ls = new Line[c.seg.length()];
			int i = 0;
			for(Line l : c.seg) {
				ls[i++] = l;
			}
			insertInner(c, ls, ts);
		}
		Circuit outermost = null;
		for(Circuit c : outer) {
			boolean isOutermost = true;
			for(CircuitTree t : ts) {
				if(insertOuter(c, t)) {
					isOutermost = false;
					break;
				}
			}
			if(isOutermost) {
				if(outermost != null) {
					throw new RuntimeException("There are two outermosts; add outer circuit");
				}
				outermost = c;
			}
		}
		if(outermost == null) {
			throw new RuntimeException("There are no outermost (no circuit?)");
		}

		Vector<Area> as = new Vector<>();
		for(CircuitTree t : ts) {
			traverseTree(t, as);
		}
		return new Pair<>(outermost, as.toArray(new Area[0]));
	}
}
