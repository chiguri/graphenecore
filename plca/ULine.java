package info.chiguri.graphene.plca;

public class ULine implements PLCAobject {
	public final Line l, inv;
	public ULine(Line l, Line inv) {
		if(l.from == inv.to && inv.from == l.to) {
			this.l = l;
			this.inv = inv;
		}
		else {
			throw new RuntimeException("Given lines are not inverse ones");
		}
	}

	@Override
	public void focused() {
		l.focused();
		inv.focused();
	}

	@Override
	public void unfocused() {
		l.unfocused();
		inv.unfocused();
	}

}
