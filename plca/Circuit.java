package info.chiguri.graphene.plca;

public class Circuit implements PLCAobject {
	Segment seg;

	public Circuit(Line[] lines) {
		seg = new Segment(lines);
		if(seg.start != seg.end) {
			throw new RuntimeException("Circuit should have the same start/end points");
		}
		if(seg.length() < 3) {
			throw new RuntimeException("Circuit should consists with three or more lines");
		}
	}

	public Circuit(Segment seg) {
		this.seg = seg;
		if(seg.start != seg.end) {
			throw new RuntimeException("Circuit should have the same start/end points");
		}
		if(seg.length() < 3) {
			throw new RuntimeException("Circuit should consists with three or more lines");
		}
	}

	public Segment.SegmentIterator segmentIterator() {
		return seg.iterateFrom(0);
	}

	public Segment.SegmentIterator segmentIterator(int n) {
		return seg.iterateFrom(n);
	}

	@Override
	public void focused() {
		seg.focused();
	}

	@Override
	public void unfocused() {
		seg.unfocused();
	}
}
