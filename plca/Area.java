package info.chiguri.graphene.plca;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class Area implements PLCAobject, Iterable<Circuit> {
	protected LinkedList<Circuit> circuits = new LinkedList<>();

	public Area(Circuit c) {
		circuits.add(c);
	}
	public Area(Circuit[] cs) {
		for(Circuit c : cs) {
			circuits.add(c);
		}
	}

	public void add(Circuit c) {
		circuits.add(c);
	}

	public void remove(Circuit c) {
		circuits.remove(c);
	}

	public boolean contains(Circuit c) {
		return circuits.contains(c);
	}

	public Area parseArea() {
		return null;
	}

	public HashMap<String, Circuit> map() {
		HashMap<String, Circuit> map = new HashMap<>();
		for(Circuit c : circuits) {
			map.put(c.toString(), c);
		}
		return map;
	}

	@Override
	public void focused() {
		for(Circuit c : circuits) {
			c.focused();
		}
	}

	@Override
	public void unfocused() {
		for(Circuit c : circuits) {
			c.unfocused();
		}
	}

	@Override
	public Iterator<Circuit> iterator() {
		return circuits.iterator();
	}
}
