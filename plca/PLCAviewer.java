package info.chiguri.graphene.plca;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import info.chiguri.swing.LeftDoubleClickedMouse;
import info.chiguri.swing.graphene.ClickedMoveMouse;
import info.chiguri.swing.graphene.GPanel;
import info.chiguri.swing.graphene.PNGSaveTrimmingEventHandler;
import info.chiguri.swing.plca.ObjectSelectPanel;

public class PLCAviewer {
	static final int LEFT = 20, RIGHT = 680, TOP = 20, BOTTOM = 680;

	static Expression testExpression(String[] args) {
		if(args.length >= 1) {
			String filename = args[0];
			if(filename.equals("-i")) {
				// どうするかな？ファイル名を受け取る？
			}
			try {
				File file = new File(filename);
				return new Expression(Files.readAllLines(file.toPath()).toArray(new String[0]));
			} catch (FileNotFoundException e) {
				System.err.println("The file " + filename + " cannot be opened");
				e.printStackTrace();
				System.exit(1);
			} catch (IOException e) {
				System.err.println("The file " + filename + " cannot be opened or broken as text");
				e.printStackTrace();
				System.exit(1);
			}
		}
		return new Expression(10);
	}

	static abstract class MenuAdaptor implements MenuListener {
		@Override
		public void menuDeselected(MenuEvent e) {
			// do nothing
		}

		@Override
		public void menuCanceled(MenuEvent e) {
			// do nothing
		}
	}

	static JMenuBar generateMenuBar(Expression exp) {
		JMenuBar bar = new JMenuBar();

		JMenu select = new JMenu("Select");
		select.addMenuListener(new MenuAdaptor() {
			boolean visible = false;
			@Override
			public void menuSelected(MenuEvent e) {
				if(!visible) {
					JFrame select = new JFrame();
					select.setSize(300, 500);
					select.addWindowListener(new WindowAdapter() {
						@Override
						public void windowClosing(WindowEvent e) {
							visible = false;
						}
					});
					visible = true;
					select.add(new ObjectSelectPanel(exp));
					select.setVisible(true);
				}
			}
		});

		JMenu renum = new JMenu("Renumber");
		renum.addMenuListener(new MenuAdaptor() {
			@Override
			public void menuSelected(MenuEvent e) {
				System.out.println("renumbering...");
				exp.renumber();
				exp.printPLCA(System.out);
			}
		});

		bar.add(select);
		bar.add(renum);
		return bar;
	}

	public static void main(String[] args) {
		final Expression exp = testExpression(args);
		JFrame m = new JFrame();
		m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		displayExpression(exp, m);
	}
	public static void displayExpression(Expression exp, final JFrame m) {
		exp.printPLCA(System.out);

		m.setSize(LEFT+RIGHT, BOTTOM+TOP);
		m.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exp.printPLCA(System.out);
			}
		});
		m.setJMenuBar(generateMenuBar(exp));
		GPanel<Point, Line> panel = new GPanel<>(exp.g);
		panel.addMouseAdapter(new LeftDoubleClickedMouse(new PNGSaveTrimmingEventHandler<Point, Line>(panel, exp.g, DisplayConfig.PointRadius+2) {
			@Override
			protected void saveImage(String filename, BufferedImage img) {
				super.saveImage(filename, img);
				try {
					filename += ".txt";
					exp.printPLCA(new PrintStream(filename));
					System.out.println("Save Text Expression as " + filename );
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}));
		panel.addMouseAdapter(new ClickedMoveMouse<>(exp.g));
		m.add(panel);
		m.setVisible(true);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				m.repaint();
			}
		}, 100, 100);

	}
}
