# Graphene: Java Graph Library for Visualization #

### How to use ###
You add this library as submodule of your project, or make a jar file from this repository.
The path should be like: 'src/info/chiguri/graphene'.
See GrapheneSwing repository if you want to see one of use cases of this library.
